from django.contrib.auth.models import User
from django.db import models


# Model for user's profiles, and the data they contain
class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(max_length=250)
    location = models.CharField(max_length=250)

    # Convert Profile objects to a string
    def __str__(self):

        return self.user.username
