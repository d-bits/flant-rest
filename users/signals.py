from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Profile


# Create a new instance of Profile, anytime a new user is created.
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):

    print("Created:", created)

    if created:

        Profile.objects.create(user=instance, role="Your Role", location="Your City")


# Save the new Profile instance to the db
def save_profile(sender, instance, **kwargs):

    instance.profile.save()
