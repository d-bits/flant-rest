from rest_framework.serializers import StringRelatedField, ModelSerializer
from django.contrib.auth.models import User
from users.models import Profile


# Serializer for the Profile model
class ProfileSerializer(ModelSerializer):

    # user = StringRelatedField(read_only=True)

    class Meta:

        model = Profile
        fields = "__all__"


# Serializer for the User model
class UserSerializer(ModelSerializer):

    # user = StringRelatedField(read_only=True)

    class Meta:

        model = User
        fields = ("username", "email", "password", "date_joined")