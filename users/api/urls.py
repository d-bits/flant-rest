from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ProfileViewSet, UsersViewSet


# Define URLs, and corresponding viewsets using DRF routers
router = DefaultRouter()
router.register(r"profiles", ProfileViewSet)
router.register(r"users", UsersViewSet)

urlpatterns = [
    path("", include(router.urls))
]
