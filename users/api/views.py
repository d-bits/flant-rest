from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from rest_framework.mixins import UpdateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin
from users.models import Profile
from .serializers import ProfileSerializer, UserSerializer
from .permissions import IsOwnProfileOrReadOnly, IsOwnerOrReadOnly
from django.contrib.auth.models import User


# CRUD viewset for User model
class UsersViewSet(
    GenericViewSet, 
    RetrieveModelMixin, 
    UpdateModelMixin, 
    ListModelMixin, 
    DestroyModelMixin
):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


# CRUD viewset for Profile model
class ProfileViewSet(
    GenericViewSet, 
    RetrieveModelMixin, 
    UpdateModelMixin, 
    ListModelMixin, 
    DestroyModelMixin
):

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated, IsOwnProfileOrReadOnly]
