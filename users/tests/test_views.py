from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from json import loads

from users.api.serializers import ProfileSerializer, UserSerializer
from users.models import Profile


# TODO: Write unit tests for views


# Test various API endpoints
class ViewTests(APITestCase):

    # Test user registration
    def test_registration(self):

        data = {
            "username": "testuser",
            "email": "test@example.com",
            "password1": "bad_pass",
            "password2": "bad_pass",
        }

        response = self.client.post("/api/auth/registration/", data=data)
        self.assertEqual(response.status_code, HTTP_201_CREATED)

    # Test that a user can login properly
    def test_login(self):

        data = {
            "username": "testuser",
            "password": "testpass",
        }

        # Create a test user, and token
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = Token.objects.create(user=self.user)

        response = self.client.post('/api/auth/login/', data=data)
        self.assertEqual(response.status_code, HTTP_200_OK)

    # Test updating a User's account info (email address)
    def test_update(self):

        data = {
            "username": "testuser",
            "password": "testpass",
            "email": "testuser@example.com",
        }

        # Create a test user, and token
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = Token.objects.create(user=self.user)

        response = self.client.put('/api/users/1/', data=data["email"])

    # Test GET requests for all users 
    def test_users(self):

        data = {
            "username": "testuser",
            "password": "testpass",
        }

        # Create a test user, and token
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = Token.objects.create(user=self.user)

        response = self.client.get("/api/users/")
        self.assertEqual(response.status_code, HTTP_200_OK)

    # Test GET requests for all users 
    def test_profiles(self):

        data = {
            "username": "testuser",
            "password": "testpass",
        }

        # Create a test user, and token
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = Token.objects.create(user=self.user)
        # Log the test user in
        self.client.post('/api/auth/login/', data=data)

        response = self.client.get("/api/profiles/")
        self.assertEqual(response.status_code, HTTP_200_OK)