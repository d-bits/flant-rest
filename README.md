
# Flant REST API

The core REST API for the Flant webinar system.

## Installed Requirements

The following software needs to be installed prior to cloning the repository:

- Python (v.3.8.3, or higher)
- PostgreSQL (v.12.2, or higher)

## Local Setup

*Instructions for running the API locally.*

- Clone the repository.
- Create a virutalenv in the root of the project with `python -m venv env`.
- Activate the virutalenv.
    * `env/scripts/activate` on Windows.
    * `source env/bin/activate` on Unix-like systems.
- `cd` into the `core` directory.
- Run `pip install -r requirements.txt` to install necessary packages.
- Create a `.env` file in the same directory as `settings.py` to store environment variables.
- Add the following environment variables to the `.env` file:
    * `DEBUG`: Set to `True` in development for better error reporting. ALWAYS set to `False` in production.
    * `SECRET_KEY`: A random, 60-character, alphanumeric string, with special characters.
    * `TIMEZONE`: A `pytz` valid time zone. Ex: `America/Vancouver` for PST.
    * `DB_NAME`: The name of database that you are using for the project in your local Postgres server.
    * `DB_HOST`: Set to `localhost`.
    * `DB_USER`: Your Postgres username.
    * `DB_PORT`: The port number that your Postgres server listens on. Default is `5432`.
    * `DB_PASS`: The password for your `DB_USER`.

**.env file has already been added to gitignore.*

## Database Migrations

Next, you will need to run database migrations to create tables in the db:

- As always, be sure your `venv` is active.
- To run database migrations, and setup the database, first make sure you are in the same directory as `manage.py`.
- Run `python manage.py makemigrations` to stage migrations
- Then run `python manage.py migrate` to write the migrations to the database schema.
- To boot the development server and use the API, run `python manage.py runserver`. 

## Creating an Admin User

Follow these steps to create a Django admin (superuser) user account, for the Django admin interface:

- Activate your virtualenv.
- `cd` into the `core` directory, where `manage.py` is located.
- Run `python manage.py createsuperuser`, and enter appropriate values into the prompts.
- To access the admin web interface, run `python manage.py runserver` to boot the dev server, and then navigate to `localhost:8000/admin` in your web browser.

## API Endpoints

You can bootstrap the development server (see above directions) to preview the API and its data from a web browser. Below are the endpoints (URLs) for the API:

- Create new user: `/api/auth/registration/`. Verbs: `GET`, `POST`
- Login existing user: `/api/auth/login/`. Verbs: `POST`
<br>
<br>
*Authentication required for the following endpoints:*
- Get list of users: `/api/users/`. Verbs: `GET`
- Get, update, or delete, a specific user: `/api/users/<int:pk>/`. Verbs: `GET`, `PUT`, `DELETE`.
- Get all profiles: `/api/profiles/`. Verbs: `GET`
- Get, update, or delete, a specific profile: `/api/profiles/<int:pk>/`. Verbs: `GET`, `PUT`, `DELETE`.